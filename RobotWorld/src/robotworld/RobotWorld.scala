package robotworld

import robotworld.gui.RobotWorldFrame
import swing.SimpleSwingApplication
import util.Random.nextInt

object RobotWorld extends SimpleSwingApplication{
  def top = MyWorld.getFrame
}

class Robot {
  private val frame = new RobotWorldFrame
  private val map = frame.panel.map

  val mapH = map.height
  val mapW = map.width

  def getFrame: RobotWorldFrame = frame

  case class Direction(d: String)

  private var posX = map.squareX
  private var posY = map.squareY

  private val NORTH = 1
  private val EAST  = 2
  private val SOUTH = 3
  private val WEST  = 4
  private var direction = nextInt(4)+1

  var paintSquare = false
  var paintColor  = RobotWorldFrame.getColor(0,0,0)

  def getColor(r: Int, g: Int, b: Int) = RobotWorldFrame.getColor(r,g,b)
  def getNextInt(max: Int) = nextInt(max)


  def paintFill() {
    if(paintSquare) map.mapSquares(posX)(posY).color = paintColor
    map.fillSquare(posX, posY)
  }

  def tryStep(): Boolean = {
    def mv(x: Int, op: String) = op match {case "-" => x-1 case "+" => x+1}

    val canStep = direction match {
      case EAST if (posX+1 < mapW) => posX = mv(posX, "+"); true
      case WEST if (posX-1 >= 0) => posX = mv(posX, "-"); true
      case NORTH if (posY-1 >= 0) => posY = mv(posY, "-"); true
      case SOUTH if (posY+1 < mapH) => posY = mv(posY, "+"); true
      case _ => false
    }

    if (canStep){
      paintFill()
      return true
    }
    false
  }

  def teleport(x: Int, y: Int)  {
    posX = x; posY = y
    paintFill()
  }

  //turn left
  def < : Robot = {
    direction = (direction) - 1
    if(direction == 0) direction = 4
    this
  }
}




