package robotworld.gui

import swing._
import event.ButtonClicked
import java.awt.{Dimension, Toolkit}
import javax.swing.border.{LineBorder, EmptyBorder}
import swing.Color
import robotworld.MyWorld
import util.Random.nextInt

/**
 * User: gurel
 * Date: 1/27/13
 * Time: 2:26 AM
 */

class RobotWorldFrame extends MainFrame {
  title = "RobotWorld"
  size = RobotWorldFrame.screenSize
  preferredSize = RobotWorldFrame.screenSize

  val panel = new RobotWorldBorderPanel

  contents = panel
}

object RobotWorldFrame {
  val screenSize = Toolkit.getDefaultToolkit.getScreenSize
  val screenWidth = screenSize.getWidth.toInt
  val screenHeight = screenSize.getHeight.toInt

  val westWidth = 200
  val southWidth = RobotWorldFrame.screenWidth
  val southHeight = 150
  val centerWidth = RobotWorldFrame.screenWidth - westWidth
  val centerHeight = RobotWorldFrame.screenHeight - southHeight - 100

  def getColor(r: Int, g: Int, b: Int) = new Color(r,g,b)
}

class RobotWorldBorderPanel extends GridBagPanel {

  class WestPanel extends BoxPanel(Orientation.Vertical) {

    preferredSize = new Dimension(RobotWorldFrame.westWidth, RobotWorldFrame.screenHeight)
    border = new EmptyBorder(20, 20, 20, 20)
    contents += new Label {
      text = "Kontroller"
    }
    contents += new Button {
      preferredSize = new Dimension(100, 50)
      text = "Go()"
      reactions += {
        case ButtonClicked(b) =>
          new Thread(new Runnable() {
            override def run() {
              b.enabled = false
              MyWorld.go()
              b.enabled = true
            }
          }).start()
      }
    }
    contents += new Button {
      preferredSize = new Dimension(100, 50)
      text = "Clean"
    }
  }

  class SouthPanel extends FlowPanel {
    preferredSize = new Dimension(RobotWorldFrame.southWidth, RobotWorldFrame.southHeight)
    contents += new Label {
      text = "Log"
    }
  }

  class RobotMap() extends Panel {

    val cellSize = 1

    background = new Color(255, 255, 255)
    border = new LineBorder(new Color(0), 1)

    var width  = 0
    var height = 0

    val adjustedDimensions = {
      val modW = RobotWorldFrame.centerWidth % cellSize
      val modH = RobotWorldFrame.centerHeight % cellSize
      width = (RobotWorldFrame.centerWidth - modW) / cellSize
      height = (RobotWorldFrame.centerHeight - modH) / cellSize
      new Dimension(RobotWorldFrame.centerWidth - modW + 1, RobotWorldFrame.centerHeight - modH + 1)
    }

    preferredSize = adjustedDimensions
    maximumSize = preferredSize
    minimumSize = preferredSize

    var squareX = nextInt(width)
    var squareY = nextInt(height)

    def fillSquare(x: Int, y: Int) {
      squareX = x
      squareY = y
      //repaint()
      repaint(new Rectangle(MapSquare.getPos(x-1), MapSquare.getPos(y-1), cellSize*3, cellSize*3))
      //Thread.sleep(1)
    }

    private object MapSquare {
      def getPos(p: Int) = p * cellSize
      def getPosX(ms: MapSquare): Int = getPos(ms.getX)
      def getPosY(ms: MapSquare): Int = getPos(ms.getY)
    }

    class MapSquare(x: Int, y: Int) {
      def getX = x
      def getY = y

      var color = new Color(235, 255, 255)
    }

    val mapSquares = Array.tabulate(width,height)((i, j) =>  new MapSquare(i, j))
    //println(mapSquares)

    override def paintComponent(g2d: Graphics2D) {
      super.paintComponent(g2d)

      g2d.setColor(new Color(200, 200, 200))

      //horizontal lines
      for (i <- 0.until(RobotWorldFrame.centerHeight) if (i % cellSize) == 0) {
        g2d.drawLine(0, i, RobotWorldFrame.centerWidth, i)
      }
      //vertical lines
      for (i <- 0.until(RobotWorldFrame.centerWidth) if (i % cellSize) == 0)
        g2d.drawLine(i, 0, i, RobotWorldFrame.centerHeight)

      def color(m: MapSquare) {
        g2d.setColor(m.color)
        g2d.fillRect(MapSquare.getPosX(m), MapSquare.getPosY(m), cellSize, cellSize)
      }

      mapSquares.map(_.map(color))

      g2d.setColor(new Color(0, 250, 0))
      g2d.fillRect(MapSquare.getPos(squareX), MapSquare.getPos(squareY), cellSize, cellSize)
    }
  }


  val map = new RobotMap
  val controlPanel = new WestPanel
  val subPanel = new SouthPanel

  val c = new this.Constraints()
  c.anchor = GridBagPanel.Anchor.PageStart
  c.gridx = 0
  c.gridy = 0
  c.gridheight = 2
  add(controlPanel, c)

  c.gridheight = 1
  c.gridx = 1
  c.gridy = 0
  c.fill = GridBagPanel.Fill.Horizontal
  add(map, c)

  c.gridx = 1
  c.gridy = 1
  add(subPanel, c)
}
