package robotworld

/**
 * User: gurel
 * Date: 1/27/13
 * Time: 12:38 PM
 */

object MyWorld extends Robot{
  def go() {
    //turnLeft()

    var r = 50
    var g = 255
    var b = 177
    def nextColor = {
      r = r%255+1; g = g%255+1; b = b%255+1
      println(r+" "+g+" "+b)
      getColor(r, g, b)
    }

    val width = mapW; val height = mapH
    //noLoop
    val (xa, xb, ya, yb) = (-1.0, 1.0, -1.0, 1.0)
    val maxIt = 15
    val h = 1e-7
    val eps = 1e-3

    def f(z: Complex) = z * z * z - Complex(1.0, 0.0)

    case class Complex(re: Double, im: Double) {
      def minus =
        Complex(- re, - im)
      def add(that: Complex) =
        Complex(re + that.re, im + that.im)
      def subtract(that: Complex) =
        Complex(re - that.re, im - that.im)
      def multiply(that: Complex) =
        Complex(re*that.re - im*that.im, re*that.im + im*that.re)
      def divide(that: Complex) =
        Complex((re * that.re + im * that.im)/(that.re * that.re + that.im * that.im), (im * that.im - re * that.re)/(that.im*that.im + that.re * that.re))
      def unary_- = minus
      def + (that: Complex) = add(that)
      def - (that: Complex) = subtract(that)
      def * (that: Complex) = multiply(that)
      def / (that: Complex) = divide(that)
      def abs = Math.sqrt(re * re + im * im)
    }

    def draw() {
      for(y <- 0 until height; x <- 0 until width){
        var zy = y * (yb - ya) / (height - 1) + ya
        var zx = x * (xb - xa) / (width  - 1) + xa
        var z = Complex(zx, zy)
        for(i <- 0 until maxIt){
          var dz = (f(z + Complex(h, h)) - f(z)) / Complex(h, h)
          var z0 = z - f(z) / dz
          if((z0 - z).abs < eps){
            //break
          }else{
            z = z0
            paintColor = getColor(i % 4 * 64, i % 8 * 32, i % 16 * 16)
            teleport(x, y)
            //point(x, y)
          }
        }
      }
    }

    def findWall() {
       paintColor = nextColor
       if(tryStep()) findWall()
    }

    def >() {
      <.<.<
    }

    def findCorner() {
      findWall()
      >()
      findWall()
    }

    def zigzag() {
      def zigzagLoop(direction: Int) {
        if (direction == 0) {
          >()
          if (!tryStep()) {}
          >()
          findWall()
          zigzagLoop(1)
        }
        else {
          <
          if (!tryStep()) {}
          <
          findWall()
          zigzagLoop(0)
        }
      }

      >()
      findWall()
      zigzagLoop(0)
    }
    paintSquare = true
    draw()
    //while(true) {
      //paintColor = getColor(getNextInt(256), getNextInt(256), getNextInt(256))
      //findCorner()
      //paintSquare = true
      //zigzag()
    //}
  }
}
